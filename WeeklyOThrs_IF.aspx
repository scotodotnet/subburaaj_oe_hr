﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WeeklyOThrs_IF.aspx.cs" Inherits="WeeklyOThrs_IF" Title="Monthly Salary OT" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Monthly Salary OT</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Monthly Salary OT</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Monthly Salary OT</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-2">
								<div class="form-group">
								  <label>Year</label>
								  <asp:DropDownList runat="server" ID="ddlYear" class="form-control select2" style="width:100%;" AutoPostBack="true"
								    OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlYear" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                            <div class="col-md-2">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="Jan">Jan</asp:ListItem>
								 <asp:ListItem Value="Feb">Feb</asp:ListItem>
								 <asp:ListItem Value="Mar">Mar</asp:ListItem>
								 <asp:ListItem Value="Apr">Apr</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="Jun">Jun</asp:ListItem>
								 <asp:ListItem Value="Jul">Jul</asp:ListItem>
								 <asp:ListItem Value="Aug">Aug</asp:ListItem>
								 <asp:ListItem Value="Sep">Sep</asp:ListItem>
								 <asp:ListItem Value="Oct">Oct</asp:ListItem>
								 <asp:ListItem Value="Nov">Nov</asp:ListItem>
								 <asp:ListItem Value="Dec">Dec</asp:ListItem>
							 	 </asp:DropDownList>
							 	  <asp:RequiredFieldValidator ControlToValidate="ddlMonth" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                               
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								<asp:TextBox runat="server" ID="txtTokenNo" class="form-control" 
                                       AutoPostBack="true" ontextchanged="txtTokenNo_TextChanged"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                           
                           <!-- end col-4 -->
                          
                          
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                            <div class="col-md-4">
                            <div class="form-group">
                           <label>Machine ID</label>
                          
                           <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                    style="width:100%;" AutoPostBack="true" 
                                    onselectedindexchanged="txtMachineID_SelectedIndexChanged" ></asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="txtMachineID" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
                          </div>
                         </div>
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                             </div>
                             <div class="row">
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Designation</label>
								<asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                             <div class="col-md-4">
							   <div class="form-group">
								<label>Week Off</label>
								<asp:Label runat="server" ID="txtWeekoff" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                             <div class="col-md-4">
							   <div class="form-group">
								<label>Month OT Hrs</label>
								<asp:Label runat="server" ID="lblMonthOTHrs" class="form-control" BackColor="#F3F3F3" Text="0.0"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                         <!-- begin row -->
                         <div class="row">
                            <div class="col-md-2">
							   <div class="form-group">
								<label>Weekoff Day</label>
								<asp:Label runat="server" ID="txtWeekoff_Day_All" class="form-control" BackColor="#F3F3F3" Text=""></asp:Label>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day1</label>
									<asp:TextBox runat="server" ID="txtDay1" class="form-control" AutoPostBack="true" ontextchanged="txtDay1_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay1" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day2</label>
									<asp:TextBox runat="server" ID="txtDay2" class="form-control" AutoPostBack="true" ontextchanged="txtDay2_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay2" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day3</label>
									<asp:TextBox runat="server" ID="txtDay3" class="form-control" AutoPostBack="true" ontextchanged="txtDay3_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay3" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day4</label>
									<asp:TextBox runat="server" ID="txtDay4" class="form-control" AutoPostBack="true" ontextchanged="txtDay4_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay4" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day5</label>
									<asp:TextBox runat="server" ID="txtDay5" class="form-control" AutoPostBack="true" ontextchanged="txtDay5_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay5" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day6</label>
									<asp:TextBox runat="server" ID="txtDay6" class="form-control" AutoPostBack="true" ontextchanged="txtDay6_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay6" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day7</label>
									<asp:TextBox runat="server" ID="txtDay7" class="form-control" AutoPostBack="true" ontextchanged="txtDay7_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay7" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day8</label>
									<asp:TextBox runat="server" ID="txtDay8" class="form-control" AutoPostBack="true" ontextchanged="txtDay8_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay8" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day9</label>
									<asp:TextBox runat="server" ID="txtDay9" class="form-control" AutoPostBack="true" ontextchanged="txtDay9_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay9" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-1">
							    <div class="form-group">
								    <label>Day10</label>
									<asp:TextBox runat="server" ID="txtDay10" class="form-control" AutoPostBack="true" ontextchanged="txtDay10_TextChanged"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDay10" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
							        <label>Absent Day</label>
								    <asp:Label runat="server" ID="txtAbsent_Day_All" class="form-control" BackColor="#F3F3F3" Text=""></asp:Label>
								</div>
                            </div>
                         </div>
                         
                         <asp:Panel ID="Panel_OT" runat="server" Visible="false">
                          <div class="row">
                             <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>OT Date</label>
										<asp:TextBox runat="server" ID="txtOTDate" class="form-control datepicker"  placeholder="dd/MM/YYYY" AutoPostBack="true" OnTextChanged="txtOTDate_Click" ></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtOTDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ControlToValidate="txtOTDate" Display="Dynamic"  ValidationGroup="Validate_FieldView" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtOTDate" ValidChars="0123456789/">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>OT Hours</label>
										<asp:TextBox runat="server" ID="txtOTHours" AutoPostBack="true"  class="form-control" 
                                            ontextchanged="txtOTHours_TextChanged"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtOTHours" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtOTHours" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                
                               <!-- end col-4 -->
                          </div>
                       <!-- end row -->  
                       </asp:Panel>
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnAdd" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnAdd_Click"/>
                                         <asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                      
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <%--<th>Machine ID</th>--%>
                                                <th>Token No</th>
                                                <th>Emp Name</th>
                                                <th>Department</th>
                                                <th>Week Off</th>
                                                <th>Tot.OTHrs</th>
                                                <th>OT Day</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <%--<td><%# Eval("MachineID")%></td>--%>
                                        <td><%# Eval("Token_No")%></td>
                                        <td><%# Eval("EmpName")%></td>
                                        <td><%# Eval("DeptName")%></td>
                                        <td><%# Eval("WeekOff")%></td>
                                        <td><%# Eval("Month_Tot_OTHrs")%></td>
                                        <td><%# Eval("OT_Date_All")%></td>
                                        
                                        <td>
                                        <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument='Edit' CommandName='<%# Eval("Token_No")%>'>
                                        </asp:LinkButton>
                                       
                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                          Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='Delete' CommandName='<%# Eval("Token_No")%>' 
                                         CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this OT details?');">
                                        </asp:LinkButton>
                                        </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<%--<asp:Button runat="server" id="btnView" Text="View" class="btn btn-primary" 
                                        ValidationGroup="Validate_FieldView" onclick="btnView_Click" />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click"/>--%>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->   
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

