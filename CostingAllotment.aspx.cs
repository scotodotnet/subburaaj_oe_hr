﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class CostingAllotment : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();


    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
           


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
                Load_Location();
                ddlunit.SelectedValue = SessionLcode;
            }
           


        }
    }
    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();


    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        //ResponseHelper.Redirect("DeptAllotmentDisplay.aspx");
        ResponseHelper.Redirect("CostingAllotmentDisplay.aspx?unit=" + ddlunit.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text, "_blank", "");
    }

}
