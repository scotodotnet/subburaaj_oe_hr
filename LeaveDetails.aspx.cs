﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class LeaveDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Manual Leave";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");


            Load_Data_EmpDet();
        }
        Load_Data();
    }

    public void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Machine_No,ExistingCode,EmpName,FromDate,ToDate,TotalDays,";
        query = query + "(case LeaveStatus when '1' then 'Approved' When '2' then 'Cancelled' else 'Pending' end) as LeaveStatus";
        query = query + " from Leave_Register_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        if (txtMachineID.SelectedValue != "-Select-")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtExistingCode.Text = DT.Rows[0]["ExistingCode"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtExistingCode.Text = "";

            }
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtExistingCode.Text = "";
        }
    }

    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        DayDiff();
    }

    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        DayDiff();
    }

    private void DayDiff()
    {
        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            DateTime Date1 = Convert.ToDateTime(txtFromDate.Text);
            DateTime Date2 = Convert.ToDateTime(txtToDate.Text);

            int daycount = (int)((Date2 - Date1).TotalDays);
            int daysAdded = daycount + 1;

            txtTotDays.Text = daysAdded.ToString();
        }
        else
        {
            txtTotDays.Text = "0";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date and To Date correctly..!');", true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();

        if (txtTotDays.Text == "" || txtTotDays.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Leave Total Days...!');", true);
        }

       //Check Leave Table
        SSQL = "Select * from Leave_Register_Mst where Machine_No='" + txtMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "' and FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            if (DT.Rows[0]["LeaveStatus"].ToString() == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details get Approved..');", true);
            }
            else if (DT.Rows[0]["LeaveStatus"].ToString() == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details get Cancelled..');", true);
            }
            
        }
        if (!ErrFlag)
        {
            SSQL = "Delete from Leave_Register_Mst where Machine_No='" + txtMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' and FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from Leave_History where Machine_No='" + txtMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' and FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            //Insert Query Here
            string MachineID_Encrypt = UTF8Encryption(txtMachineID.SelectedItem.Text);
            SSQL = "Insert Into Leave_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
            SSQL = SSQL + ",FromDate,ToDate,TotalDays,LeaveType,LeaveDesc,LeaveStatus,From_Date_Dt,To_Date_Dt) Values('" + SessionCcode + "'";
            SSQL = SSQL + ",'" + SessionLcode + "','" + txtMachineID.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + txtMachineID.SelectedItem.Text + "'";
            SSQL = SSQL + ",'" + MachineID_Encrypt + "','" + txtEmpName.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "'";
            SSQL = SSQL + ",'" + txtTotDays.Text + "','" + ddlLeaveType.SelectedItem.Text + "','" + txtLeaveDesc.Text + "','N','" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);



            DateTime Date1 = Convert.ToDateTime(txtFromDate.Text);
            DateTime Date2 = Convert.ToDateTime(txtToDate.Text);

            int daycount = (int)((Date2 - Date1).TotalDays);
            int daysAdded = daycount + 1;

            for (int intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
            {
                string Date_Value_Str = Convert.ToDateTime(txtFromDate.Text).AddDays(intCol).ToString("dd/MM/yyyy");

                SSQL = "Insert Into Leave_History(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
                SSQL = SSQL + ",FromDate,ToDate,TotalDays) Values('" + SessionCcode + "'";
                SSQL = SSQL + ",'" + SessionLcode + "','" + txtMachineID.SelectedItem.Text + "','" + txtExistingCode.Text + "','" + txtMachineID.SelectedItem.Text + "'";
                SSQL = SSQL + ",'" + MachineID_Encrypt + "','" + txtEmpName.Text + "','" + Date_Value_Str + "','" + txtToDate.Text + "'";
                SSQL = SSQL + ",'" + txtTotDays.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details Saved Successfully.');", true);
            Clear_All_Field();
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = "";
        txtExistingCode.Text = "";
        txtFromDate.Text= "";
        txtToDate.Text = ""; txtTotDays.Text="0";
        txtLeaveDesc.Text="";
        ddlLeaveType.SelectedValue = "-Select-";
        Load_Data();
        btnSave.Text = "Save";
    }
    protected void txtExistingCode_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        if (txtExistingCode.Text != "")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And IsActive='Yes' And ExistingCode='" + txtExistingCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";

                //txtTokenNo.Text = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not Data found.!');", true);
            }
        }
        else
        {
            txtExistingCode.Text = "";
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
        }
       
    }
}
