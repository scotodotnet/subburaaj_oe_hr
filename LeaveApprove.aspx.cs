﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveApprove : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Leave Approval";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            
        }
        Load_Data();

    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Leave_Register_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "' And LeaveStatus='N'";
        
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT=new DataTable();
        DataTable DT_Check=new DataTable();
        string[] LeaveDate;
        LeaveDate = e.CommandArgument.ToString().Split(',');

        string FromDate = LeaveDate[0].ToString();
        string ToDate = LeaveDate[1].ToString();

        SSQL = "Select * from Leave_Register_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
        SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "' And LeaveStatus='N'";
        DT=objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Update Leave_Register_Mst set LeaveStatus='1'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
            SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details Approved Succesfully..!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check the Employee Leave Details.!');", true);
        }
        Load_Data();
    }

    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
        string[] LeaveDate;
        LeaveDate = e.CommandArgument.ToString().Split(',');

        string FromDate = LeaveDate[0].ToString();
        string ToDate = LeaveDate[1].ToString();

        SSQL = "Select * from Leave_Register_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
        SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "' And LeaveStatus='N'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Update Leave_Register_Mst set LeaveStatus='2'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
            SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details Cancelled Succesfully..!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check the Employee Leave Details.!');", true);
        }
        Load_Data();
    }

}
