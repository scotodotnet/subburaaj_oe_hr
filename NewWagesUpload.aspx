﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="NewWagesUpload.aspx.cs" Inherits="NewWagesUpload" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

    <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Increment Upload</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Increment Upload</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
			    <asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Increment Upload</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" style="width:100%;" 
								    onselectedindexchanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<br />
								 <asp:FileUpload ID="FileUpload" runat="server" />
								</div>
								</div>
								 <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <div class=row>
                            <asp:Panel ID="Wages_Panel" runat="server" Visible="false">
                                <asp:GridView ID="GVWages" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMachineID" runat="server" Text='<%# Eval("MachineID") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>TokenNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="btnTokenNo" runat="server" Text='<%# Eval("ExistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DeptName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("FirstName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>OLD Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblWages" runat="server" Text='<%# Eval("BaseSalary") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>New Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblNewWages" runat="server" Text='<%# Eval("BaseSalary") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                    </Columns>
                                                                </asp:GridView>
                            </asp:Panel>
                       </div>
                     
                      
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnUpload" Text="Upload" class="btn btn-success" onclick="btnUpload_Click"/>
									<asp:Button runat="server" id="btnSalaryDownload" Text="Salary Download" class="btn btn-success" 
									    onclick="btnSalaryDownload_Click"/>
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
                </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                    <asp:PostBackTrigger ControlID="btnSalaryDownload" />
                 </Triggers>
              </asp:UpdatePanel> 
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</asp:Content>

