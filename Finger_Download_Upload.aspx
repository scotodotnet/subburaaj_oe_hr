﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Finger_Download_Upload.aspx.cs" Inherits="Finger_Download_Upload" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
               }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Bio Metric</a></li>
				<li class="active">Finger Donwload & Upload</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Finger Donwload & Upload</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Finger Donwload & Upload</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Download IP Address</label>
								  <asp:DropDownList runat="server" ID="ddlDownLoad_IPAddress" class="form-control select2" style="width:100%;">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlDownLoad_IPAddress" Display="Dynamic" InitialValue="-Select-"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                          <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
								        <label>Upload IP Address</label>
								        <asp:DropDownList runat="server" ID="ddlUpload_IPAddress" class="form-control select2" style="width:100%;">
								        </asp:DropDownList>
								        <asp:RequiredFieldValidator ControlToValidate="ddlUpload_IPAddress" Display="Dynamic" InitialValue="-Select-"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
								    </div>
								 <%--<div class="form-group">
									<br />
									<asp:Button runat="server" id="btndownload" Text="Download" class="btn btn-success" onclick="btndownload_Click" OnClientClick="ProgressBarShow();"/>
									<asp:Button runat="server" id="btndownloadClear" Text="Download/Clear" class="btn btn-danger" onclick="btndownloadClear_Click"/>
								 </div>--%>
                               </div>
                              <!-- end col-4 -->
                             
                              </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Machine ID</label>
								    <asp:TextBox runat="server" ID="txtMachineID" class="form-control"></asp:TextBox>
								    <asp:RequiredFieldValidator ControlToValidate="txtMachineID" Display="Dynamic" ValidationGroup="ValidateMachineID_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
							    <br />
								    <asp:LinkButton runat="server" id="btnMachineIDAdd" Text="Add" class="btn btn-success" ValidationGroup="ValidateMachineID_Field" OnClick="btnMachineIDAdd_Click"/>
							    </div>
                            </div>
                        </div>
                        
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>MachineID</th>
                                                <th>Status</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("MachineID")%></td>
                                        <td><%# Eval("Status")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("MachineID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Department details?');">
                                            </asp:LinkButton>
                                        </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					<!-- table start -->
					<asp:Panel ID="Finger_Grid_Panel" runat="server">
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>MachineID</th>
                                                <th>Name</th>
                                                <th>FingerIndex</th>
                                                <th>tmpData</th>
                                                <th>Privilege</th>
                                                <th>Password</th>
                                                <th>Enabled</th>
                                                <th>Flag</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("MachineID")%></td>
                                        <td><%# Eval("Name")%></td>
                                        <td><%# Eval("FingerIndex")%></td>
                                        <td><%# Eval("tmpData")%></td>
                                        <td><%# Eval("Privilege")%></td>
                                        <td><%# Eval("Password")%></td>
                                        <td><%# Eval("Enabled")%></td>
                                        <td><%# Eval("Flag")%></td>
                                        <td><%# Eval("Status")%></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					</asp:Panel> 
					<!-- table End -->
					
					<div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnDownload" Text="Download" class="btn btn-success" 
                                        ValidationGroup="ValidateDept_Field" onclick="btnDownload_Click" OnClientClick="ProgressBarShow();" />
                                    <asp:Button runat="server" id="btnUpload" Text="Upload" class="btn btn-success" 
                                        ValidationGroup="ValidateDept_Field" onclick="btnUpload_Click" OnClientClick="ProgressBarShow();"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        
                          <!-- begin row -->
                          <div class="row">
                          <asp:Label ID="lblDwnCmpltd" runat="server" Font-Bold="True" Font-Italic="True" 
                                    Font-Size="Medium" ForeColor="Red"></asp:Label>
                          </div>
                          
                          
                        <!-- end row -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                         <div id="Download_loader" style="display:none"/></div>
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

