﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
public partial class EmployeeFullProfileMD : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();


    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
                Load_Location();
                ddlunit.SelectedValue = SessionLcode;
                Drop_EmpCode();
            }



        }
    }

    public void Drop_EmpCode()
    {
        DataTable dt = new DataTable();

        string SSQL;

        SSQL = "";
        SSQL = "Select ExistingCode +'->'+CONVERT(varchar(10), MachineID) +'->'+ FirstName As EmpName From Employee_Mst";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " and LocCode='" + ddlunit.SelectedValue + "' And IsActive='Yes' ";
        
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And Eligible_PF='1'";
        }

        //If UCase(mvarUserType) = UCase("IF User") Then
        //    SSQL = SSQL & " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'"
        //    SSQL = SSQL & " And PFNo <> 'NULL'"
        //End If

        //If mUserLocation <> "" Then
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And IsNonAdmin='1'";
        //}

        // End If

        SSQL = SSQL + " Order By EmpNo";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlEmpName.Items.Clear();
            ddlEmpName.Items.Add("- select -");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlEmpName.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
    }
    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();


    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        
        //ResponseHelper.Redirect("DeptAllotmentDisplay.aspx");
        ResponseHelper.Redirect("EmployeeFullProfileDisplay.aspx?Empcode=" + ddlEmpName.SelectedItem.Text + "&unit=" + ddlunit.SelectedItem.Text, "_blank", "");
    }
    protected void ddlunit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Drop_EmpCode();
    }
}
