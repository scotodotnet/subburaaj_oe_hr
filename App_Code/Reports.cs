﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Reports
/// </summary>
    public static class ReportsValues
    {

           
        public static string ReportName
        {
            get { return HttpContext.Current.Session["reportnameRpt"] as string; }
            set { HttpContext.Current.Session["reportnameRpt"] = value; }
        }

        public static string PaySlipForMonth
        {
            get { return HttpContext.Current.Session["rptpayslip"] as string; }
            set { HttpContext.Current.Session["rptpayslip"] = value; }
        }
        
        
       
        

        

        public static string SancYear
        {
            get { return HttpContext.Current.Session["sancyearRpt"] as string; }
            set { HttpContext.Current.Session["sancyearRpt"] = value; }
        }

        public static string DCBillNo
        {
            get { return HttpContext.Current.Session["dcbillnoRpt"] as string; }
            set { HttpContext.Current.Session["dcbillnoRpt"] = value; }
        }

        public static string HeadOfAccount
        {
            get { return HttpContext.Current.Session["HeadAccountRpt"] as string; }
            set { HttpContext.Current.Session["HeadAccountRpt"] = value; }
        }

        public static void removeReportValues()
        {
            HttpContext.Current.Session.Remove("reportnameRpt");
            HttpContext.Current.Session.Remove("districtRpt");
            HttpContext.Current.Session.Remove("talukRpt");
            HttpContext.Current.Session.Remove("DistrictHostelRpt");
            HttpContext.Current.Session.Remove("TalukHostelRpt");
            HttpContext.Current.Session.Remove("collegecodeRpt");
            HttpContext.Current.Session.Remove("sancyearRpt");
            HttpContext.Current.Session.Remove("dcbillnoRpt");
            HttpContext.Current.Session.Remove("headaccountRpt");
        }
}






    


    public class ReportsUtilities
    {
        public static string getReportTitle(string rpt)
        {
            string rep_name = "";
            switch (rpt)
            {            
                case "rptpayslip":
                    rep_name = ""; break;
                case "dcbill_anx1":
                    rep_name = ""; break;
                case "dcbill_anx2":
                    rep_name = ""; break;
                //case "cheque_reg":
                // //   rep_name = Resources.strings.replink_chequeRegister; break;
                //case "cheque_recon":
                //    rep_name = "Reciept and Tresaury Cheques"; break;
                //case "schsanc_all":
                // //   rep_name = Resources.strings.replink_sancList; break;
                //case "schsanc_cashreg":
                //    rep_name = Resources.strings.replink_talukSancList; break;
                //case "schsanc_col":
                //    rep_name = Resources.strings.replink_collSancList; break;
                //case "schsanc_hostel":
                //    rep_name = Resources.strings.replink_collSancList; break;                
                //case "schsanc_abs":
                //    rep_name = Resources.strings.replink_studAbsReg; break;
                //case "schreject_all":
                //    rep_name = Resources.strings.replink_rejectedAppl; break;
                //case "stdreg_all":
                //    rep_name = Resources.strings.replink_studReg; break;
                case "stdreg_col":
                    rep_name = "Student Registration for Year - College Wise"; break;
                case "stdreg_colabs":
                    rep_name = "Student Registration for Year - College Wise Abstract"; break;
                case "stdreg_dup":
                    rep_name = "Student Registration for Year - Duplicates"; break;                
                case "stdreg_dupmatched":
                    rep_name = "Student Registration for Year - Duplicates with Matched Details"; break;
                case "stdsch_count":
                    rep_name = " Scholarship Students Count for the Year"; break;                               
            }
            return rep_name;
        }
    }



  
