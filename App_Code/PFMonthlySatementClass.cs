﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PFMonthlySatementClass
/// </summary>
public class PFMonthlySatementClass
{
    private string _VPF;
    private string _EPS;
    private string _EPF;
    private string _EmpNo;
    private string _EmpName;
    private string _PFNumber;
    private string _WorkedDays;
    private string _Basic;
    private string _PF;

	public PFMonthlySatementClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }

    }
    public string PF
    {
        get { return _PF; }
        set { _PF = value; }
    }
    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }
    public string PFNumber
    {
        get { return _PFNumber; }
        set { _PFNumber = value; }
    }
    public string WorkedDays
    {
        get { return _WorkedDays; }
        set { _WorkedDays = value; }
    }
    public string Basic
    {
        get { return _Basic; }
        set { _Basic = value; }
    }
    public string VPF
    {
        get { return _VPF; }
        set { _VPF = value; }
    }
    public string EPS
    {
        get { return _EPS; }
        set { _EPS = value; }
    }
    public string EPF
    {
        get { return _EPF; }
        set { _EPF = value; }
    }
}
