﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for resignClass
/// </summary>
public class resignClass
{
    private string _category;
    private string _department;
    private string _EmpNo;
    private string _EmpName;
    private string _Exist;
    private string _designation;
    private string _experience;
    private string _EligibleESI;
    private string _pfAcc;
    private string _esiAcc;
    private string _pfAmt;
    private string _ESIAmt;
    private string _username;
    private string _reason;
    private string _Ccode;
    private string _Lcode;
    private string _Advance;
	public resignClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string Category
    {
        get { return _category; }
        set { _category = value; }
    }
    public string department
    {
        get { return _department; }
        set { _department = value; }
    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }
    public string Exist
    {
        get { return _Exist; }
        set { _Exist = value; }
    }
    public string designation
    {
        get { return _designation; }
        set { _designation = value; }
    }
    public string Experience
    {
        get { return _experience; }
        set { _experience = value; }
    }
    public string EligibleESI
    {
        get { return _EligibleESI; }
        set { _EligibleESI = value; }
    }
    public string pfacc
    {
        get { return _pfAcc; }
        set { _pfAcc = value; }
    }
    public string esiacc
    {
        get { return _esiAcc; }
        set { _esiAcc = value; }
    }
    public string pfamt
    {
        get { return _pfAmt; }
        set { _pfAmt = value; }
    }
    public string ESiamt
    {
        get { return _ESIAmt; }
        set { _ESIAmt = value; }
    }
    public string Username
    {
        get { return _username; }
        set { _username = value;}
    }
    public string Reason
    {
        get { return _reason; }
        set { _reason = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string Advance
    {
        get { return _Advance; }
        set { _Advance = value; }
    }
}

