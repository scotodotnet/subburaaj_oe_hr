﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for UserRegistrationClass
/// </summary>
public class UserRegistrationClass
{
	public UserRegistrationClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _UserCode;
    private string _UserName;
    private string _Password;
    private string _Isadmin;
    private string _Mobile;
    private string _Department;
    private string _Designation;
    private string _Ccode;
    private string _Lcode;

    public string UserCode
    {
        get { return _UserCode; }
        set { _UserCode = value; }
    }
    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }

    }
    public string Password
    {
        get { return _Password; }
        set { _Password = value; }
    }
    public string IsAdmin
    {
        get { return _Isadmin; }
        set { _Isadmin = value; }
    }
    public string Mobile
    {
        get { return _Mobile; }
        set { _Mobile = value; }
    }
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }
    public string Designation
    {

        get { return _Designation; }
        set { _Designation = value; }

    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }



}
