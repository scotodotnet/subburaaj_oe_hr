﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RO_Expenses_Entry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: RO OnDuty Expenses";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_RO_Name();
            Load_Agent_Name();
            Load_TransactionID();
            if (Session["RO_TransID"] != null)
            {
                txtTransid.Text = Session["RO_TransID"].ToString();
                txtTransid.Enabled = false;
                btnSearch_Click(sender, e);
            }
        }
    }
    public void Load_RO_Name()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtROName.Items.Clear();
        query = "Select ROName from MstRecruitOfficer";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtROName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtROName.DataTextField = "ROName";
        txtROName.DataValueField = "ROName";
        txtROName.DataBind();
    }

    public void Load_Agent_Name()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtAgentName.Items.Clear();
        query = "Select Distinct AgentName from MstAgent";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentName"] = "-Select-";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtAgentName.DataTextField = "AgentName";
        txtAgentName.DataValueField = "AgentName";
        txtAgentName.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT_Q = new DataTable();

        SSQL = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Q.Rows.Count != 0)
        {
            txtTransid.Text = DT_Q.Rows[0]["TransID"].ToString();
            txtTrans_Date.Text = DT_Q.Rows[0]["Trans_Date"].ToString();
            txtROName.SelectedValue = DT_Q.Rows[0]["RO_Name"].ToString();
            txtStartDate.Text = DT_Q.Rows[0]["Start_Date_Str"].ToString();
            txtEndDate.Text = DT_Q.Rows[0]["End_Date_Str"].ToString();
            txtNo_of_Days.Text = DT_Q.Rows[0]["No_Of_Days"].ToString();
            txtPlaceOfVisit.Text = DT_Q.Rows[0]["Place_Of_Visit"].ToString();
            txtVehicle_No.Text = DT_Q.Rows[0]["Vehicle_No"].ToString();
            txtDriverName.Text = DT_Q.Rows[0]["Driver_Name"].ToString();
            txtTotalKM_Run.Text = DT_Q.Rows[0]["Tot_KM_Run"].ToString();
            txtOwnFuel_Consumed.Text = DT_Q.Rows[0]["Own_Fuel_Consumed"].ToString();
            txtOutSideFuel_Consumed.Text = DT_Q.Rows[0]["OutSide_Fuel_Consumed"].ToString();
            txtFuel_Cost_Own.Text = DT_Q.Rows[0]["Own_Fuel_Cost"].ToString();
            txtFuel_Cost_OutSide.Text = DT_Q.Rows[0]["OutSide_Fuel_Cost"].ToString();
            txtBoarding_Lodging_Expenses.Text = DT_Q.Rows[0]["Boarding_Lodging_Cost"].ToString();
            txtToll_Gate_Fees.Text = DT_Q.Rows[0]["Toll_Gate_Fee"].ToString();
            txtAgent_Expenses.Text = DT_Q.Rows[0]["Agent_Expenses"].ToString();
            txtAgentName.SelectedValue = DT_Q.Rows[0]["Agent_Name"].ToString();
            txtAgent_MobileNo.Text = DT_Q.Rows[0]["Agent_Mobile_No"].ToString();
            txtTotal_Expenses.Text = DT_Q.Rows[0]["Total_Expenses"].ToString();
            btnSave.Text = "Update";
        }

    }
    
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("RO_TransID");
        Response.Redirect("RO_Expenses_Entry_Main.aspx");
    }
    private void Clear_All_Field()
    {
        txtTransid.Text = ""; txtTrans_Date.Text = "";
        txtROName.SelectedValue = "-Select-"; txtStartDate.Text = ""; txtEndDate.Text = ""; txtNo_of_Days.Text = "0";
        txtPlaceOfVisit.Text = ""; txtVehicle_No.Text = ""; txtDriverName.Text = "";
        txtTotalKM_Run.Text = "0"; txtOwnFuel_Consumed.Text = "0"; txtOutSideFuel_Consumed.Text = "0";
        txtFuel_Cost_Own.Text = "0"; txtFuel_Cost_OutSide.Text = "0"; txtBoarding_Lodging_Expenses.Text = "0";
        txtToll_Gate_Fees.Text = "0"; txtAgent_Expenses.Text = "0";
        txtAgentName.SelectedValue = "-Select-"; txtAgent_MobileNo.Text = ""; txtTotal_Expenses.Text = "0";
        
        Session.Remove("RO_TransID");
        Load_TransactionID();
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable DT_Q = new DataTable();
        string query = "";
        string Save_Mode = "Insert";

        if (txtTotalKM_Run.Text == "") { txtTotalKM_Run.Text = "0"; }
        if (txtOwnFuel_Consumed.Text == "") { txtOwnFuel_Consumed.Text = "0"; }
        if (txtOutSideFuel_Consumed.Text == "") { txtOutSideFuel_Consumed.Text = "0"; }
        if (txtFuel_Cost_Own.Text == "") { txtFuel_Cost_Own.Text = "0"; }
        if (txtFuel_Cost_OutSide.Text == "") { txtFuel_Cost_OutSide.Text = "0"; }
        if (txtBoarding_Lodging_Expenses.Text == "") { txtBoarding_Lodging_Expenses.Text = "0"; }
        if (txtToll_Gate_Fees.Text == "") { txtToll_Gate_Fees.Text = "0"; }
        if (txtAgent_Expenses.Text == "") { txtAgent_Expenses.Text = "0"; }


        query = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransid.Text + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            Save_Mode = "Update";
            query = "Delete from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransid.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        query = "Insert Into RO_OnDuty_Expenses(Ccode,Lcode,TransID,Trans_Date,RO_Name,Start_Date,Start_Date_Str,End_Date,End_Date_Str,No_Of_Days,";
        query = query + " Place_Of_Visit,Vehicle_No,Driver_Name,Tot_KM_Run,Own_Fuel_Consumed,OutSide_Fuel_Consumed,Own_Fuel_Cost,OutSide_Fuel_Cost,";
        query = query + " Boarding_Lodging_Cost,Toll_Gate_Fee,Agent_Expenses,Agent_Name,Agent_Mobile_No,Total_Expenses) Values('" + SessionCcode + "',";
        query = query + " '" + SessionLcode + "','" + txtTransid.Text + "','" + txtTrans_Date.Text + "','" + txtROName.SelectedValue + "','" + Convert.ToDateTime(txtStartDate.Text).ToString("yyyy/MM/dd") + "',";
        query = query + " '" + txtStartDate.Text + "','" + Convert.ToDateTime(txtEndDate.Text).ToString("yyyy/MM/dd") + "','" + txtEndDate.Text + "','" + txtNo_of_Days.Text + "',";
        query = query + " '" + txtPlaceOfVisit.Text + "','" + txtVehicle_No.Text + "','" + txtDriverName.Text + "','" + txtTotalKM_Run.Text + "','" + txtOwnFuel_Consumed.Text + "',";
        query = query + " '" + txtOutSideFuel_Consumed.Text + "','" + txtFuel_Cost_Own.Text + "','" + txtFuel_Cost_OutSide.Text + "','" + txtBoarding_Lodging_Expenses.Text + "',";
        query = query + " '" + txtToll_Gate_Fees.Text + "','" + txtAgent_Expenses.Text + "','" + txtAgentName.SelectedValue + "','" + txtAgent_MobileNo.Text + "','" + txtTotal_Expenses.Text + "')";
        objdata.RptEmployeeMultipleDetails(query);

        if (Save_Mode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('RO ON Duty Expenses Deatils Updated Successfully..');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('RO ON Duty Expenses Deatils Saved Successfully..');", true);
        }

        Clear_All_Field();
    }

    protected void txtAgentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select * from MstAgent where AgentName='" + txtAgentName.SelectedValue + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            txtAgent_MobileNo.Text = DT_Q.Rows[0]["Mobile"].ToString();
        }
    }

    protected void txtEndDate_TextChanged(object sender, EventArgs e)
    {
        if (txtStartDate.Text.ToString() != "" && txtEndDate.Text.ToString() != "")
        {
            DateTime Date1 = Convert.ToDateTime(txtStartDate.Text);
            DateTime Date2 = Convert.ToDateTime(txtEndDate.Text);

            int daycount = (int)((Date2 - Date1).TotalDays);
            int daysAdded = daycount + 1;

            txtNo_of_Days.Text = daysAdded.ToString();
        }
        else
        {
            txtNo_of_Days.Text = "0";
        }
    }

    protected void txtStartDate_TextChanged(object sender, EventArgs e)
    {
        txtEndDate_TextChanged(sender, e);
    }

    protected void txtFuel_Cost_Own_TextChanged(object sender, EventArgs e)
    {
        Total_Cost_Calculate();
    }

    protected void txtFuel_Cost_OutSide_TextChanged(object sender, EventArgs e)
    {
        Total_Cost_Calculate();
    }

    protected void txtBoarding_Lodging_Expenses_TextChanged(object sender, EventArgs e)
    {
        Total_Cost_Calculate();
    }

    protected void txtToll_Gate_Fees_TextChanged(object sender, EventArgs e)
    {
        Total_Cost_Calculate();
    }

    protected void txtAgent_Expenses_TextChanged(object sender, EventArgs e)
    {
        Total_Cost_Calculate();
    }

    private void Total_Cost_Calculate()
    {
        string Fuel_Cost_Own = "0";
        string Fuel_Cost_Outside = "0";
        string Boarding_Lodging_Cost = "0";
        string Toll_Gate_Cost = "0";
        string Agent_Cost = "0";
        string Total_Cost = "0";

        if (txtFuel_Cost_Own.Text.ToString() != "") { Fuel_Cost_Own = txtFuel_Cost_Own.Text.ToString(); }
        if (txtFuel_Cost_OutSide.Text.ToString() != "") { Fuel_Cost_Outside = txtFuel_Cost_OutSide.Text.ToString(); }
        if (txtBoarding_Lodging_Expenses.Text.ToString() != "") { Boarding_Lodging_Cost = txtBoarding_Lodging_Expenses.Text.ToString(); }
        if (txtToll_Gate_Fees.Text.ToString() != "") { Toll_Gate_Cost = txtToll_Gate_Fees.Text.ToString(); }
        if (txtAgent_Expenses.Text.ToString() != "") { Agent_Cost = txtAgent_Expenses.Text.ToString(); }

        Total_Cost = (Convert.ToDecimal(Fuel_Cost_Own) + Convert.ToDecimal(Fuel_Cost_Outside)).ToString();
        Total_Cost = (Convert.ToDecimal(Total_Cost) + Convert.ToDecimal(Boarding_Lodging_Cost)).ToString();
        Total_Cost = (Convert.ToDecimal(Total_Cost) + Convert.ToDecimal(Toll_Gate_Cost)).ToString();
        Total_Cost = (Convert.ToDecimal(Total_Cost) + Convert.ToDecimal(Agent_Cost)).ToString();

        txtTotal_Expenses.Text = Total_Cost;
    }

    private void Load_TransactionID()
    {
        string query = "";
        string Transaction_ID = "0";
        DataTable DT_Q = new DataTable();
        query = "Select * from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' Order by TransID Desc";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            Transaction_ID = DT_Q.Rows[0]["TransID"].ToString();
            Transaction_ID = (Convert.ToDecimal(Transaction_ID) + Convert.ToDecimal(1)).ToString();
        }
        else
        {
            Transaction_ID = "1";
        }

        txtTransid.Text = Transaction_ID;
    }
}

