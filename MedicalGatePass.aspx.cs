﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MedicalGatePass : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL; string SessionTransNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Medical Gate Pass";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Initial_Data_Referesh();
            Load_Data_EmpDet();
            Load_Data_Reason();
            Load_Trans_ID();
            if (Session["TransID"] == null)
            {

            }
            else
            {
                SessionTransNo = Session["TransID"].ToString();
                txtTransID.Text = SessionTransNo;
                btnSearch_Click(sender, e);
            }

        }

        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search DirectPurchase
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
           
            //JobWork_Main_Sub Table Load

            DataTable dt = new DataTable();
            query = "Select * from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }


    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_Data_Reason()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtReason.Items.Clear();
        query = "Select * from MstMedical where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtReason.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Reason"] = "-Select-";
        dr["Reason"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtReason.DataTextField = "Reason";
        txtReason.DataValueField = "Reason";
        txtReason.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And SubCatName='INSIDER'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtTokenNo.DataTextField = "ExistingCode";
        txtTokenNo.DataValueField = "ExistingCode";
        txtTokenNo.DataBind();
    }

    protected void txtTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
         string query;
        DataTable DT = new DataTable();
        if (txtTokenNo.SelectedItem.Text != "-Select-")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And ExistingCode='" + txtTokenNo.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtMachineID.Text = DT.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                txtMachineID.Text = "";
            }
        }
        else
        {
            txtTokenNo.SelectedValue = "-Select-";
            txtMachineID.Text = "";
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        
    }

    private void Clear_All_Field()
    {
        txtTransID.Text = "";
        txtDate.Text = "";
        txtTokenNo.SelectedValue = "-Select-";
        txtMachineID.Text = ""; txtReason.SelectedValue = "-Select-";
        txtReason.SelectedValue = "-Select-";
        txtVehicle_No.Text = "";
        Initial_Data_Referesh();
        Session.Remove("TransID");
        Load_Trans_ID();
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;


        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Employee Details..');", true);
        }
        if (!ErrFlag)
        {
            SSQL = "Select *from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt1.Rows.Count != 0)
            {
                SSQL = "Delete from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert into Medical_GatePass(Ccode,Lcode,TransID,TransDate,TokenID,MachineID,Reason,Vehicle_No)values(";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["TokenID"].ToString() + "','" + dt.Rows[i]["MachineID"].ToString() + "','" + dt.Rows[i]["Reason"].ToString() + "','" + dt.Rows[i]["Vehicle_No"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Medical Gate Pass Details Saved Successfully..!');", true);
            Clear_All_Field();
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (!ErrFlag)
        {

            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TokenID"].ToString().ToUpper() == txtTokenNo.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This TokenNo Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["TokenID"] = txtTokenNo.SelectedValue;
                    dr["MachineID"] = txtMachineID.Text;
                    dr["Reason"] = txtReason.SelectedItem.Text;
                    dr["Vehicle_No"] = txtVehicle_No.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();


                    txtTokenNo.SelectedValue = "-Select-";
                    txtMachineID.Text = ""; txtReason.SelectedValue = "-Select-";

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["TokenID"] = txtTokenNo.SelectedValue;
                dr["MachineID"] = txtMachineID.Text;
                dr["Reason"] = txtReason.SelectedItem.Text;
                dr["Vehicle_No"] = txtVehicle_No.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();


                txtTokenNo.SelectedValue = "-Select-";
                txtMachineID.Text = ""; txtReason.SelectedValue = "-Select-";

            }
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("TokenID", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("Reason", typeof(string)));
        dt.Columns.Add(new DataColumn("Vehicle_No", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["TokenID"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_Trans_ID()
    {
        string query = "";
        DataTable DT_T = new DataTable();
        query = "Select * from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by TransID Desc";
        DT_T = objdata.RptEmployeeMultipleDetails(query);
        if (DT_T.Rows.Count != 0)
        {
            string Last_Trans_ID = DT_T.Rows[0]["TransID"].ToString();
            string Final_Trans_ID = (Convert.ToDecimal(Last_Trans_ID) + Convert.ToDecimal(1)).ToString();
            txtTransID.Text = Final_Trans_ID.ToString();
        }
        else
        {
            txtTransID.Text = "1";
        }
    }

}
