﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="RO_On_Duty_Report.aspx.cs" Inherits="RO_On_Duty_Report" Title="RO ON DUTY REPORT" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });

                $('.select2').select2();
            }
        });
    };
</script>


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Commission</a></li>
				<li class="active">RO ON DUTY REPORT</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">RO ON DUTY REPORT</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">RO ON DUTY REPORT</h4>
                        </div>
                        <div class="panel-body">
                     
								 <div class="row">
								  <div class="form-group col-md-4">
									 <label>RO NAME</label>
								     <asp:DropDownList runat="server" ID="txtRO_Name" class="form-control select2">
								     </asp:DropDownList>
								     <asp:RequiredFieldValidator ControlToValidate="txtRO_Name" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                     </asp:RequiredFieldValidator>
								 </div>
                                 <div class="form-group col-md-4">
                                    <label>From Date</label>
                          	        <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" 
                                        placeholder="dd/MM/YYYY"></asp:TextBox>
							        <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                           
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtFromDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>To Date</label>
                          	        <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" 
                                        placeholder="dd/MM/YYYY"></asp:TextBox>
							        <asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                           
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtToDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
                                 </div>
								 
								 
								  
                               </div>
                                <div class="row">
                               
                              
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report" class="btn btn-info"
									  ValidationGroup="Validate_Field"  
                                          onclick="btnReport_Click"  />
									 
								 </div>
                               </div>
                               
                              <!-- end col-4 -->
                          
                              </div>
                        <!-- end row -->
                    
                    
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>
<Triggers>
    <asp:PostBackTrigger ControlID="btnReport" />
</Triggers>

</asp:UpdatePanel>


</asp:Content>

