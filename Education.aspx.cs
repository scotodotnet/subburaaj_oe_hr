﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Education : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Education Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Data_EducationList();
            Load_Data_EmpDet();
        }
        Load_Data();
    }

    private void Load_Data_EducationList()
    {
        string query = "";
        DataTable DT = new DataTable();

        ddlPursuing.Items.Clear();
        query = "Select Education from MstEducation where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlPursuing.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Education"] = "-Select-";
        dr["Education"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlPursuing.DataTextField = "Education";
        ddlPursuing.DataValueField = "Education";
        ddlPursuing.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddlTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And SubCatName='INSIDER'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }

    protected void chkCompletion_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCompletion.Checked == true)
        {
            txtCompletionYear.Enabled = true;
        }
        else
        {
            txtCompletionYear.Enabled = false;
        }
    }
    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            lblRoomNo.Text = DT.Rows[0]["RoomNo"].ToString();
            lblMobNo.Text = DT.Rows[0]["EmployeeMobile"].ToString();
            lblQualification.Text = DT.Rows[0]["Qualification"].ToString();

        }
        else
        {
            lblRoomNo.Text = "";
            lblMobNo.Text = "";
            lblQualification.Text = "";
        }
        Load_Data();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Education_Details Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + e.CommandName.ToString() + "'";
        query = query + " And CompleteYear='' And Pursuing='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlPursuing.SelectedValue = DT.Rows[0]["Pursuing"].ToString();
            txtUniv.Text = DT.Rows[0]["University"].ToString();
            txtCourse.Text = DT.Rows[0]["Course"].ToString();
            txtDept.Text = DT.Rows[0]["Department"].ToString();
            txtStartYear.Text = DT.Rows[0]["StartYear"].ToString();
            txtRemarks.Text = DT.Rows[0]["Remarks"].ToString();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already course completed Cannot edit');", true);
        }

    }

  

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Education_Details Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        query = "Select *from Education_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " and ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Pursuing='" + ddlPursuing.SelectedItem.Text + "'";
        query = query + " and CompleteYear!=''";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('The employee completed this course...');", true);
        }

        //query = "Select *from Education_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        //query = query + " and ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Pursuing!='" + ddlPursuing.SelectedItem.Text + "'";
        //query = query + " and CompleteYear=''";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count != 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('The employee pursing course. Cannot study new one...');", true);
        //}

        if (!ErrFlag)
        {
            query = "Select *from Education_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            query = query + " and ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Pursuing='" + ddlPursuing.SelectedItem.Text + "'";
            query = query + " and CompleteYear=''";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from Education_Details where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                query = query + " and ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Pursuing='" + ddlPursuing.SelectedItem.Text + "'";
                query = query + " and CompleteYear=''";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into Education_Details (CompCode,LocCode,ExistingCode,RoomNo,EmployeeMobile,Qualification,";
            query = query + "Pursuing,Course,Department,StartYear,Complt_Chk,CompleteYear,Remarks,DisContinued_Chk,";
            query = query + "Discontinued)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + ddlTokenNo.SelectedItem.Text + "',";
            query = query + "'" + lblRoomNo.Text + "','" + lblMobNo.Text + "','" + lblQualification.Text + "',";
            query = query + "'" + ddlPursuing.SelectedItem.Text + "','" + txtCourse.Text + "','" + txtDept.Text + "',";
            query = query + "'" + txtStartYear.Text + "',";
            if (chkCompletion.Checked == true)
            {
                query = query + "'1','" + txtCompletionYear.Text + "',";
            }
            else
            {
                query = query + "'0','',";
            }
            query = query + "'" + txtRemarks.Text + "',";
            if (chkDiscontinue.Checked == true)
            {
                query = query + "'1','" + txtDisContinueRem.Text + "'";
            }
            else
            {
                query = query + "'0',''";
            }
            query = query + ")";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }

            ddlPursuing.SelectedValue = "-Select-"; txtUniv.Text = ""; txtCourse.Text = "";
            txtDept.Text = ""; txtStartYear.Text = ""; chkCompletion.Checked = false;
            txtCompletionYear.Text = ""; txtRemarks.Text = "";
            chkDiscontinue.Checked = false; txtDisContinueRem.Text = "";
            txtCompletionYear.Enabled = false;
        }
        Load_Data();
    }

    private void Clear_All_Field()
    {
        ddlTokenNo.SelectedValue = "-Select-";

        lblRoomNo.Text = ""; lblMobNo.Text = ""; lblQualification.Text = "";
        ddlPursuing.SelectedValue = "-Select-"; txtUniv.Text = ""; txtCourse.Text = "";
        txtDept.Text = ""; txtStartYear.Text = ""; chkCompletion.Checked = false;
        txtCompletionYear.Text = ""; txtRemarks.Text = "";
        chkDiscontinue.Checked = false; txtDisContinueRem.Text = "";
        txtCompletionYear.Enabled = false;
        Load_Data();
        btnSave.Text = "Save";
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
}
