﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstRecruitmentComm : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Recruitment Commission";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_CommissionType();
        }
        Load_Data();
    }

    private void Load_CommissionType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtCommType.Items.Clear();
        query = "Select *from MstCommissionType where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtCommType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CommType"] = "-Select-";
        dr["CommType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtCommType.DataTextField = "CommType";
        txtCommType.DataValueField = "CommType";
        txtCommType.DataBind();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstRecruitmentComm where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstRecruitmentComm where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and RefType='" + e.CommandName.ToString() + "' And CommType='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstRecruitmentComm where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and RefType='" + e.CommandName.ToString() + "' And CommType='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Commission Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "select * from MstRecruitmentComm where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and RefType='" + e.CommandName.ToString() + "' And CommType='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            txtRefType.SelectedValue = DT.Rows[0]["RefType"].ToString();
            txtCommType.SelectedValue = DT.Rows[0]["CommType"].ToString();
            txtAmount.Text = DT.Rows[0]["Amount"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            txtRefType.SelectedValue = "-Select-";
            txtCommType.SelectedValue = "-Select-";
            txtAmount.Text = "";

            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        
        query = "select * from MstRecruitmentComm where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and RefType='" + txtRefType.SelectedItem.Text + "' And CommType='" + txtCommType.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            SaveMode = "Update";
        }
        else
        {
            SaveMode = "Insert";
            query = "Insert into MstRecruitmentComm (CompCode,LocCode,RefType,CommType,Amount)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtRefType.SelectedItem.Text + "',";
            query = query + "'" + txtCommType.SelectedItem.Text + "','" + txtAmount.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }

        

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Commission Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Commission Saved Successfully...!');", true);
        }


        Load_Data();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtRefType.SelectedValue = "-Select-";
        txtCommType.SelectedValue = "-Select-";
        txtAmount.Text = "";

        btnSave.Text = "Save";
    }
}
