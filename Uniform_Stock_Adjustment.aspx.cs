﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_Stock_Adjustment : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL; string SessionTransNo;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Uniform Stock Adjustment";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Initial_Data_Referesh();
            Load_Data_Size();
            Load_Data_ItemName();
            Load_Trans_ID();
            if (Session["TransID"] == null)
            {

            }
            else
            {
                SessionTransNo = Session["TransID"].ToString();
                txtTransID.Text = SessionTransNo;
                btnSearch_Click(sender, e);
            }

        }

        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search DirectPurchase
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            //JobWork_Main_Sub Table Load

            DataTable dt = new DataTable();
            query = "Select ItemID,ItemName,SizeName,Add_Type,Qty,ItemRate,LineTotal,((Add_Type) + '|' + (SizeName)) as Type_SizeName from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }


    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        Total_Add();
    }

    private void Load_Data_ItemName()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtItemName.Items.Clear();
        query = "Select cast(ItemID as varchar(20)) as ItemID,ItemName from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtItemName.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemName"] = "-Select-";
        dr["ItemID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemName.DataTextField = "ItemName";
        txtItemName.DataValueField = "ItemID";
        txtItemName.DataBind();
    }

    private void Load_Data_Size()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtSize.Items.Clear();
        query = "Select * from Uniform_Size_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtSize.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SizeName"] = "-Select-";
        dr["SizeName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSize.DataTextField = "SizeName";
        txtSize.DataValueField = "SizeName";
        txtSize.DataBind();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    private void Clear_All_Field()
    {
        txtTransID.Text = "";
        txtItemID.Value = "";
        txtDate.Text = "";
        txtItemName.SelectedValue = "-Select-";
        txtSize.SelectedValue = "-Select-";
        txtRemarks.Text = "";
        txtQty.Text = "0";
        txtRate.Text = "0";
        Initial_Data_Referesh();
        Session.Remove("TransID");
        Load_Trans_ID();
        txtQtyTotal.Text = "0";
        txtTotalAmount.Text = "0";
        txtMinus_Qty_Total.Text = "0";
        txtMinus_TotalAmount.Text = "0";
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;


        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (!ErrFlag)
        {
            SSQL = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt1.Rows.Count != 0)
            {
                SSQL = "Delete from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            SSQL = "Insert into Uniform_Stock_Adjustment_Main(Ccode,Lcode,TransID,TransDate,Remarks,Qty_Total,Minus_Qty_Total,Amt_Total,Minus_Amt_Total)values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "','" + txtRemarks.Text.ToString() + "',";
            SSQL = SSQL + "'" + txtQtyTotal.Text + "','" + txtMinus_Qty_Total.Text.ToString() + "','" + txtTotalAmount.Text.ToString() + "','" + txtMinus_TotalAmount.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert into Uniform_Stock_Adjustment_Main_Sub(Ccode,Lcode,TransID,TransDate,ItemID,ItemName,SizeName,Add_Type,Qty,ItemRate,LineTotal)values(";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemID"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["SizeName"].ToString() + "','" + dt.Rows[i]["Add_Type"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["ItemRate"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (SessionAdmin == "1")
            {
                Stock_Add(txtTransID.Text.ToString());
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Details Saved Successfully..!');", true);
            Clear_All_Field();
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtQty.Text.ToString() == "0".ToString())
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Qty..');", true);
        }

        if (!ErrFlag)
        {

            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemID"].ToString().ToUpper() == txtItemName.SelectedValue.ToString().ToUpper() && dt.Rows[i]["SizeName"].ToString().ToUpper() == txtSize.SelectedValue.ToString().ToUpper() && dt.Rows[i]["Add_Type"].ToString().ToUpper() == txtStock_Type.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Details Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemID"] = txtItemID.Value;
                    dr["ItemName"] = txtItemName.SelectedItem.Text.ToString();
                    dr["SizeName"] = txtSize.Text;
                    dr["Add_Type"] = txtStock_Type.Text;
                    dr["Type_SizeName"] = txtStock_Type.SelectedValue.ToString() + "|" + txtSize.SelectedValue.ToString();
                    dr["Qty"] = txtQty.Text;
                    dr["ItemRate"] = txtRate.Text;
                    string LineTotal = (Convert.ToDecimal(txtQty.Text.ToString()) * Convert.ToDecimal(txtRate.Text.ToString())).ToString();
                    LineTotal = (Math.Round(Convert.ToDecimal(LineTotal), 2, MidpointRounding.AwayFromZero)).ToString();
                    dr["LineTotal"] = LineTotal;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();


                    txtItemName.SelectedValue = "-Select-"; txtSize.SelectedValue = "-Select-";
                    txtStock_Type.SelectedIndex = 0;
                    txtQty.Text = "0"; txtRate.Text = "0";
                    Total_Add();

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemID"] = txtItemID.Value;
                dr["ItemName"] = txtItemName.SelectedItem.Text.ToString();
                dr["SizeName"] = txtSize.Text;
                dr["Add_Type"] = txtStock_Type.Text;
                dr["Qty"] = txtQty.Text;
                dr["ItemRate"] = txtRate.Text;
                string LineTotal = (Convert.ToDecimal(txtQty.Text.ToString()) * Convert.ToDecimal(txtRate.Text.ToString())).ToString();
                LineTotal = (Math.Round(Convert.ToDecimal(LineTotal), 2, MidpointRounding.AwayFromZero)).ToString();
                dr["LineTotal"] = LineTotal;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();


                txtItemName.SelectedValue = "-Select-"; txtSize.SelectedValue = "-Select-";
                txtStock_Type.SelectedIndex = 0;
                txtQty.Text = "0"; txtRate.Text = "0";
                Total_Add();

            }
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("ItemID", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("SizeName", typeof(string)));
        dt.Columns.Add(new DataColumn("Add_Type", typeof(string)));
        dt.Columns.Add(new DataColumn("Type_SizeName", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRate", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";
        string[] Type_Size = e.CommandArgument.ToString().Split('|');
        string SizeName_Str = Type_Size[1].ToString();
        string Add_Type_Str = Type_Size[0].ToString();

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemID"].ToString() == e.CommandName.ToString() && dt.Rows[i]["SizeName"].ToString() == SizeName_Str.ToString() && dt.Rows[i]["Add_Type"].ToString() == Add_Type_Str.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_Trans_ID()
    {
        string query = "";
        DataTable DT_T = new DataTable();
        query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by TransID Desc";
        DT_T = objdata.RptEmployeeMultipleDetails(query);
        if (DT_T.Rows.Count != 0)
        {
            string Last_Trans_ID = DT_T.Rows[0]["TransID"].ToString();
            string Final_Trans_ID = (Convert.ToDecimal(Last_Trans_ID) + Convert.ToDecimal(1)).ToString();
            txtTransID.Text = Final_Trans_ID.ToString();
        }
        else
        {
            txtTransID.Text = "1";
        }
    }

    protected void txtItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtItemID.Value = txtItemName.SelectedValue;
        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select * from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + txtItemName.SelectedItem.Text + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
        //if (DT.Rows.Count != 0)
        //{
        //    txtRate.Text = DT.Rows[0]["ItemRate"].ToString();
        //}
        //else
        //{
        //    txtRate.Text = "0";
        //}
    }

    protected void txtSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Get Stock Rate
        //Get Rate
        string query = "Select cast( isnull((Sum(Add_Value)-Sum(Minus_Value)) / (Sum(Add_Qty)-Sum(Minus_Qty)),0) as decimal(18,2)) as ItemRate";
        query = query + " from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemName='" + txtItemName.SelectedItem.Text + "'";
        query = query + " And SizeName='" + txtSize.SelectedValue + "'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtRate.Text = DT.Rows[0]["ItemRate"].ToString();
        }
        else
        {
            txtRate.Text = "0";
        }
    }

    private void Stock_Add(string Transaction_No)
    {
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT_CH = new DataTable();
        query = "Select * from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            query = "Select * from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Stock Adjustment'";
            DT_CH = objdata.RptEmployeeMultipleDetails(query);
            if (DT_CH.Rows.Count != 0)
            {
                query = "Delete from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Stock Adjustment'";
                DT_CH = objdata.RptEmployeeMultipleDetails(query);
            }
            //Insert Stock Ledger
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string ItemDesc_Full = DT.Rows[i]["ItemName"].ToString() + " Size:" + DT.Rows[i]["SizeName"].ToString();
                if (DT.Rows[i]["Add_Type"].ToString() == "Add")
                {
                    query = "Insert Into Uniform_Stock_Ledger_All(Ccode,Lcode,TransID,TransDate,TransDate_Str,TransType,ItemDesc,ItemName,SizeName,Add_Qty,Add_Rate,";
                    query = query + " Add_Value,Minus_Qty,Minus_Rate,Minus_Value,Party_Name,Token_No,Wages_Type) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + Transaction_No + "',Convert(Datetime,'" + DT.Rows[i]["TransDate"].ToString() + "',103),";
                    query = query + " '" + DT.Rows[i]["TransDate"].ToString() + "','Stock Adjustment','" + ItemDesc_Full + "','" + DT.Rows[i]["ItemName"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["SizeName"].ToString() + "','" + DT.Rows[i]["Qty"].ToString() + "','" + DT.Rows[i]["ItemRate"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["LineTotal"].ToString() + "','0.00','0.00','0.00','Stock Adjustment Mill','','')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    query = "Insert Into Uniform_Stock_Ledger_All(Ccode,Lcode,TransID,TransDate,TransDate_Str,TransType,ItemDesc,ItemName,SizeName,Add_Qty,Add_Rate,";
                    query = query + " Add_Value,Minus_Qty,Minus_Rate,Minus_Value,Party_Name,Token_No,Wages_Type) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + Transaction_No + "',Convert(Datetime,'" + DT.Rows[i]["TransDate"].ToString() + "',103),";
                    query = query + " '" + DT.Rows[i]["TransDate"].ToString() + "','Stock Adjustment','" + ItemDesc_Full + "','" + DT.Rows[i]["ItemName"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["SizeName"].ToString() + "','0.00','0.00','0.00','" + DT.Rows[i]["Qty"].ToString() + "','" + DT.Rows[i]["ItemRate"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["LineTotal"].ToString() + "','Stock Adjustment Mill','','')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }
    }

    private void Total_Add()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        string Qty_Tot = "0";
        string Amt_Tot = "0";
        string Qty_Tot_Minus = "0";
        string Amt_Tot_Minus = "0";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Add_Type"].ToString() == "Add")
            {
                Qty_Tot = (Convert.ToDecimal(dt.Rows[i]["Qty"].ToString()) + Convert.ToDecimal(Qty_Tot)).ToString();
                Amt_Tot = (Convert.ToDecimal(dt.Rows[i]["LineTotal"].ToString()) + Convert.ToDecimal(Amt_Tot)).ToString();
            }
            else
            {
                Qty_Tot_Minus = (Convert.ToDecimal(dt.Rows[i]["Qty"].ToString()) + Convert.ToDecimal(Qty_Tot_Minus)).ToString();
                Amt_Tot_Minus = (Convert.ToDecimal(dt.Rows[i]["LineTotal"].ToString()) + Convert.ToDecimal(Amt_Tot_Minus)).ToString();
            }
            
        }
        txtQtyTotal.Text = Qty_Tot;
        txtTotalAmount.Text = Amt_Tot;
        txtMinus_Qty_Total.Text = Qty_Tot_Minus;
        txtMinus_TotalAmount.Text = Amt_Tot_Minus;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("Uniform_Stock_Adjustment_Main.aspx");
    }
}
